# This is a simple Makefile for :
#	- compiling classes (make classes)
#	- generating the javadoc (make doc)
# 	- rebuilding the local checker (make jar) 
# 

JAVAC = javac
JAVADOC = javadoc -d doc
JAVAFLAGS = -O -d classes -encoding utf-8
JAVACLASSPATH = -cp lib/pdfbox-app-1.8.8.jar
SRC = src/fr/imag/forge/scidetect/Checker/Utils/*.java src/fr/imag/forge/scidetect/Checker/*.java src/fr/imag/forge/scidetect/Corpus/*.java src/fr/imag/forge/scidetect/Logger/*.java src/fr/imag/forge/scidetect/SciDetect_local/*.java src/fr/imag/forge/scidetect/TextExtractor/*.java

default: all

all: classes doc jar run

classes:
	mkdir -p classes
	$(JAVAC) $(JAVAFLAGS) $(JAVACLASSPATH) $(SRC) 

doc:
	$(JAVADOC) $(SRC)

jar:
	cd classes ; jar -cfvm ../SciDetect_Local`date +%Y-%m-%d`.jar ../manifest.mf *; cd ..
	cp  SciDetect_Local`date +%Y-%m-%d`.jar SciDetect_Local.jar 

run:
	java -jar SciDetect_Local`date +%Y-%m-%d`.jar -l checklog.txt -c Test

clean:
	rm -r classes; rm -r doc;
