/*
 * Copyright (C) 2015 UNIVERSITE JOSEPH FOURIER (Grenoble 1)/ Springer-Verlag GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.imag.forge.scidetect.SciDetect_local;

import fr.imag.forge.scidetect.Checker.Classifier;
import fr.imag.forge.scidetect.Checker.Cleaner;
import fr.imag.forge.scidetect.Checker.DistantCalculator;
import fr.imag.forge.scidetect.Checker.Reader;
import fr.imag.forge.scidetect.Checker.Utils.DistancesSet;
import fr.imag.forge.scidetect.Logger.Log;
import fr.imag.forge.scidetect.Corpus.Corpus;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author Nguyen Minh Tien - minh-tien.nguyen@imag.fr
 */
public class SciDetect_Local {

    // private String loglocation;
    // private String detailloglocation;
    private String testpath;
    //private String logtime;
    public Corpus samples = new Corpus();
    //private Corpus tests = new Corpus();
    private String SamplesFolder;
    //private HashMap<String, HashMap<String, Double>> distant = new HashMap<String, HashMap<String, Double>>();
    DistancesSet distant = new DistancesSet();
    private Boolean savedetaillog = false;
    private Boolean clean = true;

    /**
     * Read in the config file: - places where to find samples of each class -
     * default places where to write results.
     *
     * @throws FileNotFoundException
     * @throws IOException
     */
    private void readconfig() throws FileNotFoundException, IOException {
        File conf = new File("config.txt");
        BufferedReader br = new BufferedReader(new FileReader(conf));
        String line;
        while ((line = br.readLine()) != null) {
            if (!line.startsWith("#")) {
                // System.out.println(line);
                String[] b = line.split("\t");
                if (b[0].equals("samples")) {
                    SamplesFolder = b[1];
                }
                if (b[0].equals("Default_log_folder")) {

                    Log.loglocation = b[1];
                    //  System.out.println(loglocation);
                }
                if (b[0].equals("Default_detail_log_folder")) {
                    Log.detailloglocation = b[1];
                    //System.out.println(detailloglocation);
                }
            }
        }

    }

    /**
     * This should be where all the components be called, It can be used as an
     * interface for a stand alone SciDetect API library.
     *
     * @throws IOException
     */
    public String compute(String[] args) throws IOException {
        readconfig();
        readargs(args);
        String conclusion = new String();
        if (testpath != null) {
            DateFormat dateFormat = new SimpleDateFormat("yyyy.MM.dd-HH:mm");
            Date date = new Date();
            Log.logtime = dateFormat.format(date);
            try {
                Reader reader = new Reader();

                reader.readconfig();
                samples = reader.readsamples(SamplesFolder);
                conclusion=reader.readtests(testpath, samples, savedetaillog);

            } catch (Exception e) {
                e.printStackTrace();
                System.out.println("* Something went wrong during:");
                System.out.println(" - reading the config file");
                System.out.println(" - or reading the samples (dir data)");
                System.out.println(" - or txt extraction from pdf");
                System.out.println("* Continuing anyway...");
                //e.printStackTrace();
            }
            if (clean) {
                Cleaner cleaner = new Cleaner();
                cleaner.clean(testpath);
            }
//            DistantCalculator dc = new DistantCalculator();
//            distant = dc.caldistant(samples, tests);
//            Classifier cl = new Classifier();
//             conclusion = cl.classify(distant);
//            System.out.println(conclusion);
//            Log log = new Log();
//            log.savelog(conclusion);
//            if (clean){
//            Cleaner cleaner = new Cleaner();
//            cleaner.clean(testpath);}
//            if (savedetaillog) {
//                log.savedetaillog(distant);
//            }
        } else {
            System.out.println("***** Can not read path to the folder:" + testpath);
            System.out.println("***** The folder should contains file to check");
        }
        return conclusion;
    }

    /**
     * Parsing of the command line arguments: where to find pdf files, where
     * results should be written
     *
     * @param args
     */
    public void readargs(String[] args) {
        if (args.length > 0) {
            for (int i = 0; i < args.length; i += 1) {
                // System.out.println(args[i]);
                if (args[i].equals("-l")) {
                    Log.loglocation = args[i + 1];
                }
                if (args[i].equals("-c")) {
                    testpath = args[i + 1];
                }
                if (args[i].equals("-d")) {
                    savedetaillog = true;
                }
                if (args[i].equals("-noclean")) {
                    clean = false;
                }
                if (args[i].equals("-h")) {
                    printUsage();
                }
            }
        } else {
            printUsage();
        }
    }

    /**
     * To print usage (-h).
     */
    private static void printUsage() {
        System.out.println("***** Scigen & Co Checker \n");
        System.out.println("To test all files in a directory <pathToFilesDirToTest>:");
        System.out.println("java -jar SciDetect_local.jar -l <pathToLogFile> -c <pathToFilesDirToTest> \n");
        System.out.println("To print usage:");
        System.out.println("java -jar SciDetect_local.jar -h \n");
        System.out.println("***** \n");
    }

    /**
     * This is the standalone checker. All pdf files in the dir specified after
     * -c are checked against classes found in the dir "data". Results are
     * written in the log file specified by the -l option. If -d is given a
     * detailled log is produced. Example: testing all pdf files in a directory
     * MyConf/PDF/ and having results in the MyConf/checklog.txt: java -jar
     * ScigenChecker_local.jar -l MyConf/checklog.txt -c MyConf/PDF/
     *
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(String[] args) throws IOException {

        SciDetect_Local a = new SciDetect_Local();
        a.compute(args);

    }

}
