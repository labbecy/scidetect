/*
 * Copyright (C) 2015 UNIVERSITE JOSEPH FOURIER (Grenoble 1)/ Springer-Verlag GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.imag.forge.scidetect.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.HashMap;

/**
 * Manages the log files, where final results are written.
 *
 * @author Nguyen Minh Tien - minh-tien.nguyen@imag.fr
 */
public class Log {

    /**
     * The location of the log File
     */
    public static String loglocation;

    /**
     * The location of the detail log file
     */
    public static String detailloglocation;

    /**
     * Time when the log was created
     */
    public static String logtime;

    /**
     * Write in a log file all the computed distances.
     *
     * @param distant The distances set
     */
    public void savedetaillog(HashMap<String, HashMap<String, Double>> distant) {
        File dloglocation = new File(detailloglocation);
        if (!dloglocation.exists()) {
            dloglocation.mkdir();
        }
        File distantout = new File(detailloglocation + logtime + ".tsv");
        //File distantout = new File(testpath+"/alldistant.xls");
        PrintWriter out;
        try {
            out = new PrintWriter(distantout);

            for (String key : distant.keySet()) {
                for (String key2 : distant.get(key).keySet()) {
                    out.println(key + "\t" + key2 + "\t"
                            + distant.get(key).get(key2));
                }
            }
            out.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    /**
     * Write in a log file the classification decision
     *
     * @param conclusion contains the txt to be written in the log
     */
    public void savelog(String conclusion) {
        File distantout;
        if (!loglocation.equals("logs/")) {
            distantout = new File(loglocation);
        } else {
            File location = new File(loglocation);
            if (!location.exists()) {
                location.mkdir();
            }
            distantout = new File(loglocation + logtime + ".tsv");
        }

        PrintWriter out;
        try {

            out = new PrintWriter(distantout);

            out.write(conclusion);

            out.close();
        } catch (FileNotFoundException e) {
            System.out.println("***** Scidetect : Output file error \n");
            System.out.println("***** Most probably the specified file is a Dir \n");
            //e.printStackTrace();
        }
    }
}
