/*
 * Copyright (C) 2015 UNIVERSITE JOSEPH FOURIER (Grenoble 1)/ Springer-Verlag GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.imag.forge.scidetect.Checker;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import fr.imag.forge.scidetect.Checker.Utils.DistancesSet;
import fr.imag.forge.scidetect.Checker.Utils.ThresholdsSet;

/**
 * Classifier is tagging input files has being of a certain class. Example of
 * classes are SCIgen, Mathgen,... The decision is made according to the
 * distance between the tested file and its nearest neighbor in each class.
 * Thresholds for assigning are read in file specified in the configuration file
 * (default config.txt).
 *
 * @author Nguyen Minh Tien - minh-tien.nguyen@imag.frs
 */
public class Classifier {

    private ThresholdsSet SetOfThresholds;

    /**
     *Build a new classifier, thresholds are read in the configuration file
     */
    public Classifier() {
        this.SetOfThresholds = new ThresholdsSet();
        this.SetOfThresholds.Init();
    }

    /**
     * Classify is classifying each document given the matrix of distances
     * (distant). For each entry it gives the class (or more) to which the text
     * can be assigned
     *
     * @param distant is a matrix of distances
     * @return the assigned class
     * @throws IOException
     */
    public String classify(DistancesSet distant) throws IOException {

        String result = "";
        String conclusion = "";

        for (String key : distant.keySet()) {
            //for each file in the test
            result = find_NN(distant.get(key));
            //System.out.println(result);
            //System.out.println(key);
            //System.out.println(result);
            String[] a = checkdistant(result).split("\n");

            if (a[0].length() == 0) {
                conclusion += key + "\t" + "cant classify\t1\tnull\n";
            } else {
                for (int i = 0; i < a.length; i++) {

                    conclusion += key + "\t" + a[i] + "\n";
                }
            }

        }
        //System.out.println(conclusion);
        return conclusion;

    }

    /**
     * Check if the distance is lower, between of upper the two threshold.
     *
     * @param result a string composed having for each classes the value of its
     * NN
     *@return a string composed of the classes and the distances to the nearest neighbor in each class.
     */
    private String checkdistant(String result) {
        String conclusion = "";
        String[] eachtype = result.split("\n");

        for (int i = 0; i < eachtype.length; i++) {
            String[] eachNN = eachtype[i].split("\t");
            //System.out.println(eachtype[i]);
            //get threshold for the corresponding type
            Double[] threshold = new Double[2];

            if (SetOfThresholds.containsKey(eachNN[0])) {
                threshold = SetOfThresholds.get(eachNN[0]);
            } else {
                threshold = SetOfThresholds.get("Default");
            }
            //check distant with threshold
            if (Double.parseDouble(eachNN[1]) < threshold[0]) {
                conclusion += eachNN[0] + "\t" + eachNN[1] + "\t" + eachNN[2] + "\n";
            } else if (Double.parseDouble(eachNN[1]) < threshold[1]) {
                conclusion += "Suppected " + eachNN[0] + "\t" + eachNN[1] + "\t" + eachNN[2] + "\n";
            }

        }
        if (conclusion == "") {
            conclusion = findmindistant(result);
        }
        return conclusion;
    }

    /**
     * In case it is not a generation, find the closest text to it in general
     *
     * @param result
     * @return
     */
    private String findmindistant(String result) {
        Double mindistant = 1.0;
        String[] eachtype = result.split("\n");

        String conclu = "";
        for (int i = 0; i < eachtype.length; i++) {
            String[] eachNN = eachtype[i].split("\t");
            if (Double.parseDouble(eachNN[1]) < mindistant) {
                mindistant = Double.parseDouble(eachNN[1]);
                conclu = "Genuine \t" + eachNN[1] + "\t" + eachNN[2] + "\n";
            }
        }
        return conclu;
    }

    /**
     * Get the type of the generation base on the path to it
     *
     * @param indexpath path to the NN
     * @return type of generation (parent folder)
     */
    private String gettype(String indexpath) {
        File indexfile = new File(indexpath);
        String parent = indexfile.getParent();
        // String type = parent.substring(0, parent.lastIndexOf("/"));
        parent = parent.substring(parent.lastIndexOf("/") + 1, parent.length());
        return parent;
    }

    /**
     * Get the nearest neighbor in each type of generation
     *
     * @param distantto
     * @return
     */
    private String find_NN(HashMap<String, Double> distantto) {
        HashMap<String, Double> distotype = new HashMap<String, Double>();
        HashMap<String, String> NNname = new HashMap<String, String>();
        Double MinNN = 1.0;
        String NN = "";

        for (String key : distantto.keySet()) {
            String type = gettype(key);
            if (!distotype.containsKey(type)) {
                distotype.put(type, distantto.get(key));
                NNname.put(type, key);
            } else if (distantto.get(key) < distotype.get(type)) {
                distotype.put(type, distantto.get(key));
                NNname.put(type, key);
            }

        }
//remove either physgen or scigen to advoid misslassification
        if (distotype.containsKey("Scigen") && distotype.containsKey("Physgen")) {
            if (distotype.get("Scigen") < distotype.get("Physgen")) {
                distotype.remove("Physgen");
            } else {
                distotype.remove("Scigen");
            }
        }
        // it returns the path to the NN
        String result = "";
        for (String key : distotype.keySet()) {
            result += key + "\t" + distotype.get(key) + "\t" + NNname.get(key) + "\n";
        }
        return result;
    }
}
