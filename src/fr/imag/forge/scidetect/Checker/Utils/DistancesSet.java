/*
 * Copyright (C) 2015 UNIVERSITE JOSEPH FOURIER (Grenoble 1)/ Springer-Verlag GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.imag.forge.scidetect.Checker.Utils;

import java.util.HashMap;

/**
 * DistancesSet(A,B) Stores the distance between the two texts A and B 
 *
 */
public class DistancesSet extends HashMap<String, HashMap<String, Double>>{
	
    /**
     * Constructor
     */
    public DistancesSet() {
	    super();
    }
    
    
    /**
     * Get the value of the distance between A and B
     * @param A text
     * @param B text
     * @return the distance between A and B
     */
    public Double getDist(String A, String B){
    	return this.get(A).get(B);
    }

    /**
     * Set the distance between A and B to the value d
     * @param A text
     * @param B text
     * @param d distant
     */
    public void setDist(String A, String B, Double d){
    	if (this.get(A) == null) {this.put(A,new HashMap<String, Double>());}
    	this.get(A).put(B,d);
    }
    
}
