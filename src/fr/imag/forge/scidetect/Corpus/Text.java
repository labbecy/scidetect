/*
 * Copyright (C) 2015 UNIVERSITE JOSEPH FOURIER (Grenoble 1)/ Springer-Verlag GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.imag.forge.scidetect.Corpus;

import java.util.HashMap;

/**
 *
 * @author tien
 */
public class Text {

    private HashMap<String, Integer> index = new HashMap<String, Integer>();
    private String cleantext = "";
    private String name = "";

    /**
     *set the index of the text
     * @param a
     */
    public void setindex(HashMap<String, Integer> a) {
        index = a;

    }

    /**
     *set the name of the text file
     * @param a
     */
    public void setname(String a) {
        name = a;

    }

    /**
     *get the index of the text
     * @return index
     */
    public HashMap<String, Integer> getindex() {
        return index;
    }
    
    /**
     *get the name of the text file 
     * @return name
     */
    public String getname() {
        return name;
    }
}
