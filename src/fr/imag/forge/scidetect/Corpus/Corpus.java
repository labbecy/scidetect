/*
 * Copyright (C) 2015 UNIVERSITE JOSEPH FOURIER (Grenoble 1)/ Springer-Verlag GmbH
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package fr.imag.forge.scidetect.Corpus;

import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author tien
 */
public class Corpus {

    private HashMap<String, Text> corpus = new HashMap<String, Text>();

    /**
     *set a new text into the corpus
     * @param text
     */
    public void put(Text text) {
        corpus.put(text.getname(), text);
    }

    /**
     *get the whole corpus
     * @return corpus
     */
    public HashMap<String, Text> getcorpus() {
        return corpus;
    }

    /**
     *get the set of key inside the corpus
     * @return set of key
     */
    public Set<String> keySet() {
        return corpus.keySet();
    }

    /**
     *get a specific index of a text inside the corpus
     * @param name of the file  
     * @return index of the file
     */
    public HashMap<String, Integer> get(String name) {
        return corpus.get(name).getindex();
    }
}
